//
//  HomeViewModelTests.swift
//  Satya_Weather_appTests
//
//  Created by Satyanarayana on 24/05/23.
//

import XCTest
@testable import Satya_Weather_app

final class HomeViewModelTests: XCTestCase {
    
    var viewModel: HomeViewModel!
        

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        viewModel = HomeViewModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewModel = nil
        super.tearDown()
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPrepareData() {
            // Given
        let mockWeather = Weather(main: "Clouds", weatherDescription: "overcast clouds", icon: "04d")
        let mockMain = Main(temp: 25.0, pressure: 1012, humidity: 70, tempMin: 284.64, tempMax: 284.64)
        let mockCoord = Coord(lon: -150.0003, lat:64.0003)

        let mockWeatherResponse = WeatherResponse(
            coord: mockCoord,
            weather: [mockWeather],
            base: "stations",
            main: mockMain,
            visibility: 10000,
            id: 5128581,
            name: "Alaska",
            cod: 200
        )

            viewModel.weatherResponse = mockWeatherResponse
            
            // When
            viewModel.prepareData()
            
            // Then
            XCTAssertEqual(viewModel.dataSource.count, 5)
            XCTAssertEqual(viewModel.dataSource[0].title, "Description:")
            XCTAssertEqual(viewModel.dataSource[0].value, "overcast clouds")
            XCTAssertEqual(viewModel.dataSource[1].title, "Min Temp:")
            XCTAssertEqual(viewModel.dataSource[1].value, "11.5°C")
            XCTAssertEqual(viewModel.dataSource[2].title, "Max Temp:")
            XCTAssertEqual(viewModel.dataSource[2].value, "11.5°C")
            XCTAssertEqual(viewModel.dataSource[3].title, "Humidity:")
            XCTAssertEqual(viewModel.dataSource[3].value, "70%")
            XCTAssertEqual(viewModel.dataSource[4].title, "Pressure:")
            XCTAssertEqual(viewModel.dataSource[4].value, "1012 hPa")
        }
    
    func testSetSearchCity() {
            // Given
            let cityName = "Alaska"
            
            // When
            viewModel.setSearchCity(searchCityName: cityName)
            
            // Then
            XCTAssertEqual(viewModel.cityName, cityName)
        }
    
    func testStoreInCache() {
            // Given
            let cityName = "Alaska"
            
            // When
            viewModel.setSearchCity(searchCityName: cityName)
            viewModel.storeInCache()
            
            // Then
            let cacheData = CityCacheData()
            let storedCityName = cacheData.getLastSearchedCity()
            XCTAssertEqual(storedCityName, cityName)
        }

}
