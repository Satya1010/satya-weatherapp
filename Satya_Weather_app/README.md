**Features**
**Location Access:** The app prompts the user to grant permission for accessing their location. If permission is granted, the app retrieves weather data based on the user's current location.

**Weather Data Retrieval:** The app retrieves weather data for US cities. Users can enter the name of a US city and retrieve the current weather information for that city.

**Image Cache:** The app includes an image caching mechanism to optimize the loading of weather icons. This helps in providing a smooth user experience by reducing network requests and improving performance.

**Auto-load Last Searched City:** Upon launching the app, it automatically loads the weather data for the last searched city, providing a seamless experience for users who frequently check the weather for the same location.

**Search Functionality:** The app allows users to search for any US city and retrieve its weather data. The search functionality enables users to quickly find weather information for their desired location.

**Cache Management:** The app implements caching of the last 10 searched cities. This allows users to easily access their recent searches and quickly retrieve weather data without performing a new search. Additionally, the app provides an option to clear the search history and remove all cached data.

**Dark Mode:** The app supports dark mode, providing a visually appealing interface for users who prefer a darker color scheme. The dark mode enhances readability and reduces eye strain in low-light environments.

**Unit Test Cases:** The app includes a comprehensive suite of unit test cases to ensure the correctness and reliability of the implemented functionalities. The unit tests cover critical components, data models, and business logic to validate the behavior of the app.
