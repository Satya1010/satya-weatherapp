//
//  WeatherResponse.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana on 24/05/23.


import Foundation

// MARK: - WeatherResponse
struct WeatherResponse: Codable {
    let coord: Coord
    let weather: [Weather]
    let base: String
    let main: Main
    let visibility, id: Int
    let name: String
    let cod: Int
    
    init(coord: Coord, weather: [Weather], base: String, main: Main, visibility: Int, id: Int, name: String, cod: Int) {
        self.coord = coord
        self.weather = weather
        self.base = base
        self.main = main
        self.visibility = visibility
        self.id = id
        self.name = name
        self.cod = cod
    }
}

// MARK: - Coord
struct Coord: Codable {
    let lon, lat: Double
    init(lon: Double, lat: Double) {
        self.lon = lon
        self.lat = lat
    }
}

// MARK: - Main
struct Main: Codable {
    let temp: Double
    let pressure, humidity: Int
    let tempMin, tempMax: Double
    
    enum CodingKeys: String, CodingKey {
        case temp, pressure, humidity
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
    
    init(temp: Double, pressure: Int, humidity: Int, tempMin: Double, tempMax: Double) {
        self.temp = temp
        self.pressure = pressure
        self.humidity = humidity
        self.tempMin = tempMin
        self.tempMax = tempMax
    }
}

// MARK: - Weather
struct Weather: Codable {
    let main, weatherDescription, icon: String
    
    enum CodingKeys: String, CodingKey {
        case main
        case weatherDescription = "description"
        case icon
    }
    
    init(main: String, weatherDescription: String, icon: String) {
        self.main = main
        self.weatherDescription = weatherDescription
        self.icon = icon
    }
}
