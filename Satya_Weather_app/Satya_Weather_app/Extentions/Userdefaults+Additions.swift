//
//  Userdefaults+Additions.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana on 24/05/23.
//

import Foundation

extension UserDefaults {

    private enum Keys {
        static let city = "cityName"
    }

    /// The last searched city stored in UserDefaults.
    /// Use this property to get or set the last searched city.
    class var lastSearchedCity: String? {
        get {
            // Retrieve the last searched city from UserDefaults
            return UserDefaults.standard.string(forKey: Keys.city)
        }
        set {
            // Set the last searched city in UserDefaults
            UserDefaults.standard.set(newValue, forKey: Keys.city)
        }
    }

}
