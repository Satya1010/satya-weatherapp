//
//  TableView+additions.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import UIKit

extension UITableView {
    func tableDefaultSettings() {
        self.rowHeight = UITableView.automaticDimension
        self.estimatedRowHeight = UITableView.automaticDimension
        self.tableFooterView = UIView()
    }
}
