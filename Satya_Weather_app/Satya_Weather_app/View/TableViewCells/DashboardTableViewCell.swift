//
//  DashboardTableViewCell.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import UIKit
import Kingfisher

class DashboardTableViewCell: UITableViewCell {

    @IBOutlet weak var temparatureLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    
    /// Method to display temparature and cityname in dashboard
    /// - Parameters:
    ///   - cityName: cityname as String
    ///   - temparature: temparature as String
    func displayData(cityName:String,temparature:String, icon: String) {
        temparatureLabel.text = temparature
        cityNameLabel.text = cityName
        let iconURLString = prepareIconURL(with: icon)

        let url = URL(string: iconURLString)
        weatherIconImageView.kf.setImage(with: url,
                                         placeholder: UIImage(named:IconName.weathericon))
    }
    
   private func prepareIconURL(with icon: String) -> String {
       return String(format: Constants.BaseURLs.iconURL, icon)
        }
}
