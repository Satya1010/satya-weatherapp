//
//  DetailsTableViewCell.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import UIKit

class DetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    var data: DetailsData? = nil {
        didSet {
            self.titleLabel.text = data?.title
            self.valueLabel.text = data?.value
        }
    }

}
