//
//  HomeViewController.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import UIKit
import CoreLocation

protocol WeatherCityDelegate: AnyObject {
    ///Method to capture cityname as String
    func displayCityName(cityName: String)
}
class HomeViewController: UIViewController,WeatherCityDelegate {
    
    struct Constants {
        static let screenTitle = "Welcome"
        static let locationDisableMessage = "Please enable location services for this app in your device settings."
        static let locationDisableTitle = "Location Services Disabled"
        static let settings = "Settings"
        static let cancel = "Cancel"
        static let error = "Error"
        static let apiError = "Something went wrong!. Please check the city name"

    }
    /// Homeview Model
    private var homeViewModel = HomeViewModel()
    @IBOutlet weak var tableView: UITableView!
    private var locationManager:CLLocationManager?
    private var shouldUpdateLocation = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Constants.screenTitle
        setupNavigationBar()
        ///Default custom tableview settings
        setupTableView()
        homeViewModel.getLastCityDetailsFromCache()
        if homeViewModel.checkValidCity(){
            fetchWeatherDataAPI()
        } else {
            tableView.reloadData()
        }
    }
    /// Adding search bar button, location bar button to navigation bar method
    private func setupNavigationBar() {
        // Create and set up search button in navigation bar
        let searchButton = createBarButtonItem(imageName: IconName.searchIcon, action: #selector(searchTapped))
        // Create and set up location button in navigation bar
        let locationButton = createBarButtonItem(imageName: IconName.locationIcon, action: #selector(locationTapped))
        // Set the right bar button items with search and location buttons
        navigationItem.rightBarButtonItems = [searchButton, locationButton]
    }
    
    private func createBarButtonItem(imageName: String, action: Selector) -> UIBarButtonItem {
        // Create a custom button with the specified image, action, and theme color
        let button = UIButton()
        button.setImage(UIImage(named: imageName), for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        button.tintColor = Colors.themeColor
        // Return a UIBarButtonItem with the custom button as the custom view
        return UIBarButtonItem(customView: button)
    }
    
    private func setupTableView() {
        // Set up default settings for the table view
        tableView.tableDefaultSettings()
        // Set the data source of the table view to the current view controller
        tableView.dataSource = self
    }
    
    private func reloadTableViewData() {
        tableView.reloadData()
    }
    /// Search button tapped method
    @objc func searchTapped() {
        if let searchViewController = self.storyboard!.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController {
            searchViewController.delegate = self
            self.present(searchViewController, animated: true, completion: nil)
        }
       
    }
    @objc private func locationTapped() {
        getUserLocation()
    }
    
    /// Get city name and prepare the API call
    /// - Parameter cityName: city name as String
    func displayCityName(cityName: String) {
        ///Store city name to cache
        homeViewModel.setSearchCity(searchCityName: cityName)
        ///Call weather API in view model
        fetchWeatherDataAPI()
    }
    
    func fetchWeatherDataAPI() {
        homeViewModel.callWeatherAPI() { [weak self] (error) in
            DispatchQueue.main.async {
                guard let self = self else { return }
                
                if error == nil {
                    self.homeViewModel.storeInCache()
                    self.reloadTableViewData()
                } else {
                    self.showAlertMessageWith(title: Constants.error, message: Constants.apiError)

                }
            }
        }
    }
    
}
///Tableview Delegate methods
extension HomeViewController:UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        ///Section 0 to display main titles, Section 1 for other details
        return section == 0 ? 1 : homeViewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardTableViewCell") as? DashboardTableViewCell else {
                return UITableViewCell()
            }
            ///Set values as City name and Temparature
            cell.displayData(cityName:homeViewModel.cityName,
                             temparature:homeViewModel.getCurrentTemparature(),
                             icon: homeViewModel.iconString)
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsTableViewCell") as? DetailsTableViewCell else {
                return UITableViewCell()
            }                ///Set data to display other weather details
            cell.data = homeViewModel.dataSource[indexPath.row]
            return cell
        }
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    
    /// Request user location permission and start updating location
    func getUserLocation() {
        shouldUpdateLocation = true
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    /// CLLocationManagerDelegate method called when the authorization status changes
    ///
    /// - Parameters:
    ///   - manager: The location manager instance
    ///   - status: The new authorization status
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            // Location permission granted, continue updating location
            locationManager?.startUpdatingLocation()
        case .denied, .restricted:
            // Location permission denied or restricted, show an alert to prompt the user to enable it
            showAlertToEnableLocationServices()
        default:
            break
        }
    }
    
    /// Show an alert to prompt the user to enable location services
    private func showAlertToEnableLocationServices() {
        let alert = UIAlertController(title: Constants.locationDisableTitle, message: Constants.locationDisableMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.settings, style: .default) { _ in
            // Open app settings
            if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            }
        })
        alert.addAction(UIAlertAction(title: Constants.cancel, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    /// CLLocationManagerDelegate method called when new location updates are available
    ///
    /// - Parameters:
    ///   - manager: The location manager instance
    ///   - locations: An array of updated locations
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard shouldUpdateLocation, let location = locations.last else { return }
        
        shouldUpdateLocation = false
        
        getCityNameFromCoordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude) { [weak self] cityName in
            guard let self = self else { return }
            
            if let cityName = cityName {
                self.displayCityName(cityName: cityName)
            } else {
                print("Failed to retrieve city name.")
            }
        }
    }
    
    /// Retrieve the city name for the given coordinates
    ///
    /// - Parameters:
    ///   - latitude: The latitude of the location
    ///   - longitude: The longitude of the location
    ///   - completion: The completion closure to be called with the retrieved city name
    func getCityNameFromCoordinates(latitude: Double, longitude: Double, completion: @escaping (String?) -> Void) {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        let geocoder = CLGeocoder()
        
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            guard let placemark = placemarks?.first, error == nil else {
                completion(nil)
                return
            }
            
            if let city = placemark.locality {
                completion(city)
            } else {
                completion(placemark.name)
            }
        }
    }
}

