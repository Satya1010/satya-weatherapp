//
//  DetailsData.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import Foundation

class DetailsData {
    var title:String = ""
    var value:String = ""
    
    fileprivate init () {
        
    }
    
    init(title:String, value:String) {
        self.title = title
        self.value = value
    }
    
}
