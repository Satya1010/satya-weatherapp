//
//  HomeViewModel.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import Foundation

class HomeViewModel {
    
   private enum VMConstants {
        static let defaultTitle = "Please search for the city"
    }
    
    var dataSource = [DetailsData]()
    fileprivate(set) var cityName = VMConstants.defaultTitle
    var temparature = 0.0
    var iconString = ""
    var weatherResponse:WeatherResponse?
    ///Temparatyre type display format
    var temparatureType:TempType = .Farenheat
    
    /// Prepare  weather details for datasorce
    func prepareData() {
        temparature = weatherResponse?.main.temp ?? 0.0
        iconString = weatherResponse?.weather.first?.icon ?? ""
        let description = DetailsData(title: "Description:", value: weatherResponse?.weather.first?.weatherDescription ?? "")
        let minTemparature =  DetailsData(title: "Min Temp:", value: Utilites.convertTemp(temparature:weatherResponse?.main.tempMin ?? 0.0, from: .kelvin, to: temparatureType.getType()))
        let maxTemparature =  DetailsData(title: "Max Temp:", value: Utilites.convertTemp(temparature: weatherResponse?.main.tempMax ?? 0.0, from: .kelvin, to: temparatureType.getType()))
        let humidity =  DetailsData(title: "Humidity:", value:"\(weatherResponse?.main.humidity ?? 0)%")
        let pressure =  DetailsData(title: "Pressure:", value:"\(weatherResponse?.main.pressure ?? 0) hPa")
        
        dataSource = [description,minTemparature,maxTemparature,humidity,pressure]
    }
    
    func setSearchCity(searchCityName: String) {
        cityName = searchCityName
    }
    
    /// Method to store city name in cache
    /// - Parameter cityName: cityname as String
    func storeInCache() {
        let cacheData = CityCacheData()
        cacheData.storeCity(name: cityName)
    }
    
    func getLastCityDetailsFromCache() {
        let cacheData = CityCacheData()
        cityName = cacheData.getLastSearchedCity() ?? VMConstants.defaultTitle
    }
    /// Method to Validate City
    func checkValidCity() -> Bool {
            if cityName.isEmpty || cityName == VMConstants.defaultTitle {
                return false
            }
            return true
        }
    
    /// Method to get temparatre in reable format
    /// - Returns: temparatre value as string
    func getCurrentTemparature() -> String {
        return  Utilites.convertTemp(temparature: temparature, from: .kelvin, to: temparatureType.getType())
    }
    /// Method to call waether API
    func callWeatherAPI(completion: @escaping (_ error:Error?) -> Void) {
        NetworkManager.sharedInstance.getWeatherAPI(paramsString: prepareInputParameters()) { (response, error) in
            
            if error == nil {
                ///Store the reponse
                self.weatherResponse = response
                ///Prepare  weather details
                self.prepareData()
                return completion(nil)
            } else {
                return completion(error)
            }
        }
    }
    
    /// Prepare URL input
    /// - Returns: input params as String
    private func prepareInputParameters() -> String {
        
        let inputString = "?q=\(cityName)&appid=\(Constants.AppKeys.ApiKey)"
        
        return inputString
    }
    
}
