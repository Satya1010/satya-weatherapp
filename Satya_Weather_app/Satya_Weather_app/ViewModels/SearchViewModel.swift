//
//  SearchViewModel.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import Foundation

class SearchViewModel {
    var cacheCities = [String]() // Array to store cached cities
    
    // Current search string
    fileprivate(set) var searchString: String = ""
    
    var dataSource: [String]? {
        get {
            if searchString.isEmpty {
                return cacheCities // Return all cached cities if the search string is empty
            }
            
            // Filter the cached cities based on the search string
            return self.cacheCities.filter { (city) -> Bool in
                return city.lowercased().contains(searchString.lowercased())
            }
        }
    }
    
    /// Update the search string with the provided value
    ///
    /// - Parameter string: The search string to be set
    func searchCities(string: String) {
        searchString = string
    }
    
    /// Fetch all cached cities and prepare the cache data
    ///
    /// This method retrieves the cities from the CityCacheData and extracts their names
    /// to populate the cacheCities array.
    func prepareCacheData() {
        cacheCities = CityCacheData().getCities().map { $0.name }
    }
    
    /// Clear the stored cache data
    ///
    /// This method clears the cache data by calling the clearCache method of CityCacheData
    /// and then re-prepares the cache data using the prepareCacheData method.
    func clearCacheData() {
        CityCacheData().clearCache()
        prepareCacheData()
    }
}

