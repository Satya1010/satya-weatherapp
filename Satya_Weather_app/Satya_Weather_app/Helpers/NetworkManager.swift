//
//  NetworkManager.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import Foundation
import Alamofire

class NetworkManager {
    
    static let sharedInstance = NetworkManager()
    
    private init() {
        // Private initializer to enforce singleton pattern
    }
    
    /// Perform API request to get weather report.
    ///
    /// - Parameters:
    ///   - paramsString: The string of parameters for the API request.
    ///   - completion: Completion block called when the request is complete with weather data or error.
    func getWeatherAPI(paramsString: String, completion: @escaping ((WeatherResponse?, Error?) -> Void)) {
        let urlString = Constants.BaseURLs.baseUrl + Constants.WeatherAPI.weather + paramsString
        
        AF.request(urlString).responseData { response in
            if let error = response.error {
                // Handle network request error
                completion(nil, error)
            } else {
                guard let responseData = response.data else {
                    // Handle missing or invalid response data
                    let error = NSError(domain: "", code: -1, userInfo: [kCFErrorLocalizedDescriptionKey as String : NSLocalizedString("Error while fetching weather data", comment:"Error while fetching weather data.")])
                    completion(nil, error)
                    return
                }
                
                // Convert response data to WeatherResponse object using utility method
                Utilites.dataToObject(type: WeatherResponse.self, from: responseData, completion: completion)
            }
        }
    }
}

