//
//  Constants.swift
//  Satya_Weather_app
//
//  Created by Satyanarayana  on 24/05/23.


import Foundation
import UIKit

struct Constants {
    
    struct BaseURLs {
        
        static let baseUrl = "https://api.openweathermap.org/"
        static let iconURL = "https://openweathermap.org/img/wn/%@@2x.png"
    }
    
    struct WeatherAPI {
        static let weather = "data/2.5/weather"
    }
    
    struct AppKeys {
        
        static let ApiKey = "b4e22c263a6a47d2c2cae911061354e3"
    }
}
///Temparate types to display as Celsius, Fahrenheit
enum TempType:Int {
    case Celsius
    case Farenheat
    
    func getType() -> UnitTemperature {
        switch self {
        case .Celsius:
            return .celsius
        case .Farenheat:
            return .fahrenheit
        }
    }
}

struct ColorSetName {
    static let themeColor = "themeColor"
    
}

struct Colors {
    static let themeColor = UIColor(named: ColorSetName.themeColor) ?? .green
    
}

struct IconName {
    static let searchIcon = "searchicon"
    static let locationIcon = "locationicon"
    static let weathericon = "weathericon"
}
